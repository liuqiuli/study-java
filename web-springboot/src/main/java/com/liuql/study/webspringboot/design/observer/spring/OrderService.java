package com.liuql.study.webspringboot.design.observer.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    @Autowired
    ApplicationContext applicationContext;

    public void sveOrder(){
        //1.保存订单
        System.out.println("保存订单");

        //2.发送短信
//        System.out.println("发送短信通知");

        //3.发送微信
//        System.out.println("发送微信通知");

        //利用spring机制实现观察者模式（发布订阅模式）
        OrderCreateEvent orderCreateEvent = new OrderCreateEvent(new Object());
        applicationContext.publishEvent(orderCreateEvent);
    }
}
