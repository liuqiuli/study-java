package com.liuql.study.webspringboot.design.strategy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//收银台
@Service
public class FeeCalculationService {
    @Autowired
    NormalDiscountStrategy normalDiscountStrategy;
    @Autowired
    VipDiscountStrategy vipDiscountStrategy;
    @Autowired
    SvipDiscountStrategy svipDiscountStrategy;

    /**
     * 收银
     * @param type  vip,svip,normal
     * @param fee 收费
     * @return
     */
    public double calculation(String type, double fee){
//        if("vip".equals(type)){
//            return fee * 0.98;
//        } else if("svip".equals(type)){
//            return fee * 0.8;
//        }else{
//            return fee * 1;
//        }

//        if("vip".equals(type)){
//            return vipDiscountStrategy.discount(fee);
//        } else if("svip".equals(type)){
//            return svipDiscountStrategy.discount(fee);
//        }else{
//            return normalDiscountStrategy.discount(fee);
//        }

        DiscountStrategy discountStrategy = strategyMapmap.get(type);
        return discountStrategy.discount(fee);
    }

    //保存策略对象
    Map<String, DiscountStrategy> strategyMapmap = new HashMap();

    //解决if else问题  由spring自动注入
    public FeeCalculationService(List<DiscountStrategy> discountStrategyList){
        for (DiscountStrategy discountStrategy : discountStrategyList) {
            strategyMapmap.put(discountStrategy.type(), discountStrategy);
        }
    }
}
