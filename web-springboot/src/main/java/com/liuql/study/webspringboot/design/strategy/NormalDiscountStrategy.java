package com.liuql.study.webspringboot.design.strategy;

import org.springframework.stereotype.Service;

@Service
public class NormalDiscountStrategy implements DiscountStrategy{
    @Override
    public String type() {
        return "normal";
    }

    @Override
    public double discount(double fee) {
        return fee * 1;
    }
}
