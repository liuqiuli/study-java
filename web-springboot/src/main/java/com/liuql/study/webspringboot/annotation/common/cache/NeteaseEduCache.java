package com.liuql.study.webspringboot.annotation.common.cache;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 网易微专业注解缓存
 * Retention 定义注解声明周期
 * Document 文档注解，会被Javadoc工具文档化
 * Inherited 是否让子类集成注解
 * Target 描述了注解的应用范围
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface NeteaseEduCache {
    String key();
}
