package com.liuql.study.webspringboot.design.strategy;

//折扣
public interface DiscountStrategy {
    public String type();
    public double discount(double fee);
}
