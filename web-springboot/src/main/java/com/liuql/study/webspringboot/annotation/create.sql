CREATE TABLE `kl_sku` (
  `sku_id` varchar(64) NOT NULL COMMENT 'sku唯一编号',
  `item_id` varchar(65) DEFAULT NULL COMMENT '关联的id',
  `price` bigint(20) DEFAULT NULL COMMENT '价格 单位:哩',
  `sku_name` varchar(255) DEFAULT NULL COMMENT 'sku名称',
  PRIMARY KEY (`sku_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;