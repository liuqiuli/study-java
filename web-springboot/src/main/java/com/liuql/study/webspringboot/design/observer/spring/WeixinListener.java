package com.liuql.study.webspringboot.design.observer.spring;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class WeixinListener implements ApplicationListener<OrderCreateEvent> {
    @Override
    public void onApplicationEvent(OrderCreateEvent orderCreateEvent) {
//        orderCreateEvent.getSource();
        System.out.println("发送微信通知");
    }
}
