package com.liuql.study.webspringboot.annotation.services;

import com.liuql.study.webspringboot.annotation.common.cache.NeteaseEduCache;
import com.liuql.study.webspringboot.annotation.dao.SkuMapper;
import com.liuql.study.webspringboot.annotation.pojo.Sku;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class SkuService {

    @Autowired
    SkuMapper skuMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    @NeteaseEduCache(key = "'netease_' + #id")
    public Sku findById(String id) {
        Sku sku = null;

        //1.从缓存中查
//        sku = (Sku) redisTemplate.opsForValue().get(id);
//        if(sku != null){
//            System.out.println("从缓存中读取到值");
//            return sku;
//        }

        //2.从数据库中查  真正业务代码 其它使用注解实现
        System.out.println("数据查询方法执行中.....");
        sku = skuMapper.find(id);

        //3.同步存储value到缓存
//        redisTemplate.opsForValue().set(id, sku);

        return sku;
    }

    public void insert(Sku sku) {
        skuMapper.insert(sku);
    }
}
