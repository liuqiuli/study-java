package com.liuql.study.webspringboot.sso;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/sso")
public class SpringSessionController {

    @RequestMapping("index")
    public Map sessions(HttpServletRequest request){
        HttpSession httpSession = request.getSession();
        Map map = new HashMap();
        map.put("class", httpSession.getClass());
        map.put("sessionId", httpSession.getId());
        return map;
    }
}
