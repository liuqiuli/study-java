package com.liuql.study.webspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudyWebSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudyWebSpringbootApplication.class, args);
    }
}
