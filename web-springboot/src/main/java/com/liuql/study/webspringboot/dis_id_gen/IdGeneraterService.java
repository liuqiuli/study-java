package com.liuql.study.webspringboot.dis_id_gen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class IdGeneraterService {

    @Autowired
    private RedisTemplate redisTemplate;

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

    private static final String genIdKey = "liuql:study:dis:id";

    public String generate(){
        long id = redisTemplate.opsForValue().increment(genIdKey);
        return sdf.format(new Date()) + id;
    }
}
