package com.liuql.study.webspringboot.annotation.common.cache;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * AOP切面编程~~~
 */
@Component
@Aspect
public class CacheAspect {
    @Autowired
    private RedisTemplate redisTemplate;

    @Around("@annotation(com.liuql.study.webspringboot.annotation.common.cache.NeteaseEduCache)")
    public Object doAnyThing(ProceedingJoinPoint joinPoint) throws Throwable {
        Object key = null;

        //0.反射技术：从注解里面，读取key的生成规则
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = joinPoint.getTarget().getClass().getMethod(signature.getName(), signature.getMethod().getParameterTypes());
        NeteaseEduCache cacheAnnotation = method.getAnnotation(NeteaseEduCache.class);
        String keyEL = cacheAnnotation.key();

        //0.1.根据key的规则，通过springEL表达式进行解析
        // 创建解析器
        ExpressionParser parser = new SpelExpressionParser();
        Expression expression = parser.parseExpression(keyEL);
        //设置解析上下文（有哪些占位符，以及每种占位符的值）
        EvaluationContext context = new StandardEvaluationContext();

        Object[] args = joinPoint.getArgs();//所有参数值
        DefaultParameterNameDiscoverer discoverer = new DefaultParameterNameDiscoverer();
        String[] parameterNames = discoverer.getParameterNames(method);
        for (int i = 0; i < parameterNames.length; i++) {
            context.setVariable(parameterNames[i], args[i].toString());
        }
        //解析
        key = expression.getValue(context).toString();

        //1.判断缓存中是否存在
        Object value = redisTemplate.opsForValue().get(key);
        if(value != null) {
            System.out.println("从缓存中取到了值");
            return value;
        }

        //2.不存在则执行数据库查询
        value = joinPoint.proceed();
        //3.存储数据到缓存中
        redisTemplate.opsForValue().set(key, value);
        return value;
    }

}
