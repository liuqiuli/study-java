package com.liuql.study.webspringboot.annotation.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class Sku implements Serializable {
    //sku id
    private String skuId;
    //item_id 对应的产品ID
    private String itemId;
    //价格 单位：哩
    private long price;
    //名称
    private String skuName;
}
