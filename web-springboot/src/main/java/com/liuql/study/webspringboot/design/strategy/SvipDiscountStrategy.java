package com.liuql.study.webspringboot.design.strategy;

import org.springframework.stereotype.Service;

@Service
public class SvipDiscountStrategy implements DiscountStrategy{
    @Override
    public String type() {
        return "svip";
    }

    @Override
    public double discount(double fee) {
        return fee * 0.88;
    }
}
