package com.liuql.study.webspringboot.manual.mvc.service;

import java.util.Map;

public interface StudyService {

    int insert(Map map);

    int delete(Map map);

    int update(Map map);

    int select(Map map);

}
