package com.liuql.study.webspringboot.annotation.dao;

import com.liuql.study.webspringboot.annotation.pojo.Sku;
import org.apache.ibatis.annotations.*;

@Mapper
public interface SkuMapper {
    @Insert("INSERT INTO `kl_sku` (`sku_id`, `item_id`, `price`, `sku_name`) VALUES (#{skuId}, #{itemId}, #{price}, #{skuName})")
    void insert(Sku sku);

    @Delete("delete from kl_sku where sku_id=#{skuId}")
    void delete(@Param("skuId") String id);

    @Select("select sku_id, item_id, price, sku_name from kl_sku where sku_id=#{skuId}")
    Sku find(@Param("skuId") String skuId);
}
