package com.liuql.study.webspringboot.design.strategy;

import org.springframework.stereotype.Service;

@Service
public class VipDiscountStrategy implements DiscountStrategy{
    @Override
    public String type() {
        return "vip";
    }

    @Override
    public double discount(double fee) {
        return fee * 0.98;
    }
}
