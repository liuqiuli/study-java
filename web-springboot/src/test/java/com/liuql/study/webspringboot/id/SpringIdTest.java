package com.liuql.study.webspringboot.id;

import com.liuql.study.webspringboot.StudyWebSpringbootApplication;
import com.liuql.study.webspringboot.dis_id_gen.IdGeneraterService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StudyWebSpringbootApplication.class)
public class SpringIdTest {
    @Autowired
    private IdGeneraterService idGeneraterService;

    private static final int threadNum = 100;

    private static CountDownLatch cdl = new CountDownLatch(threadNum);

    private static Map map = new HashMap();


    @Test
    public void test() throws InterruptedException {
        System.out.println("-----------start-----------");
        for (int i=0; i<threadNum; i++){
            new Thread(new IdThread()).start();
            cdl.countDown();
        }
        Thread.currentThread();
        Thread.sleep(3000);
        System.out.println(map.size());
        System.out.println("-----------end-----------");
    }

    class IdThread implements Runnable{
        @Override
        public void run() {
            try{
                cdl.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String key = idGeneraterService.generate();
            System.out.println(key);
            map.put(key, key);
        }
    }
}
