package com.liuql.study.webspringboot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StudyWebSpringbootApplicationTests {

    @Test
    public void contextLoads() {
        String content = "Hello World.你好世界.";
        byte[] bs = content.getBytes();
        String string = new String(bs);
    }

}
