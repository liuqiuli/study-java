package com.liuql.study.webspringboot.design.observer.spring;

import com.liuql.study.webspringboot.StudyWebSpringbootApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StudyWebSpringbootApplication.class)
public class SpringObserverTest {
    @Autowired
    private OrderService orderService;

    @Test
    public void test(){
        orderService.sveOrder();
    }
}
