package com.liuql.study.webspringboot.annotation;


import com.liuql.study.webspringboot.StudyWebSpringbootApplication;
import com.liuql.study.webspringboot.annotation.pojo.Sku;
import com.liuql.study.webspringboot.annotation.services.SkuService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StudyWebSpringbootApplication.class)
public class BenchmarkTests {

    @Autowired
    SkuService skuService;

    //高并发场景 支持2000并发操作
    @Test
    public void benchmarkTest() throws InterruptedException {
        int threadNum = 2000; //并发数量

        //J.U.C多线程并发编程包栅栏机制
        CyclicBarrier cyclicBarrier = new CyclicBarrier(threadNum);
        for (int i=0;i < threadNum; i++){
            new Thread(() ->{
                try {
                    cyclicBarrier.await();

                    Sku sku = skuService.findById("9d1d245e-ed5c-4362-90c2-7e8d1a0791fd");
                    System.out.println("查找结果" + sku.getSkuName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        }

        //测试1分钟 不管有没有直接关闭
        TimeUnit.SECONDS.sleep(60);
    }
}
