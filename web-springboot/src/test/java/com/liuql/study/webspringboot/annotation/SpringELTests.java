package com.liuql.study.webspringboot.annotation;

import org.junit.Test;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

public class SpringELTests {

    @Test
    public void test1() {
        String template = "%s : 正在测试";
        String result = String.format(template, "我是alvin");
        System.out.println(result);
    }

    @Test
    public void SPELTest() {
        String ELdemo = "'hello:' + #userId";

        //1.创建解析器
        ExpressionParser parser = new SpelExpressionParser();
        Expression expression = parser.parseExpression(ELdemo);
        //2.设置解析上下文（有哪些占位符，以及每种占位符的值）
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("userId", "alvin");
        //3.解析
        String result = expression.getValue(context).toString();
        System.out.println(result);
    }
}
