package com.liuql.study.webspringboot.annotation;

import com.liuql.study.webspringboot.StudyWebSpringbootApplicationTests;
import com.liuql.study.webspringboot.annotation.pojo.Sku;
import com.liuql.study.webspringboot.annotation.services.SkuService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StudyWebSpringbootApplicationTests.class)
public class SpringTest {

    @Autowired
    SkuService skuService;

    @Test
    public void insert() {
        Sku sku = new Sku();
        sku.setSkuId(UUID.randomUUID().toString());
        sku.setItemId("产品ID:大波浪T恤");
        sku.setPrice(9000); //9块钱
        sku.setSkuName("m码");
        skuService.insert(sku);
    }

    @Test
    public void query() {
        Sku sku = skuService.findById("9d1d245e-ed5c-4362-90c2-7e8d1a0791fd");
        System.out.print("查找结果：");
        System.out.println(sku);
    }

}
