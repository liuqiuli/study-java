package com.liuql.study.webspringboot.design.strategy;

import com.liuql.study.webspringboot.StudyWebSpringbootApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StudyWebSpringbootApplication.class)
public class StrategyTest {
    @Autowired
    private FeeCalculationService feeCalculationService;

    @Test
    public void test(){
        System.out.println("vip:"+feeCalculationService.calculation("vip", 100));
        System.out.println("svip:"+feeCalculationService.calculation("svip", 100));
        System.out.println("normal:"+feeCalculationService.calculation("normal", 100));
    }
}
